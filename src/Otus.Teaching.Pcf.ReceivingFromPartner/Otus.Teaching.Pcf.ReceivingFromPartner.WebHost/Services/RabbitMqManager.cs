﻿using System.Text;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Options;
using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services
{
    public enum MessageType
    {
        GivePromoCodeToCustomer,
        NotifyAdminAboutPartnerManagerPromoCode
    }
    public interface IRabbitMqManager
    {
        void SendMessage<T>(T message, MessageType messageType);
        void SendMessage(string message, MessageType messageType);
    }

    public class RabbitMqManager: IRabbitMqManager
    {
        private const string ExchangeName = "amq.topic";
        private const string ExchangeType = "topic";

        private readonly RabbitMqOptions _options;
        private readonly ILogger<RabbitMqManager> _logger;

        public RabbitMqManager(IOptions<RabbitMqOptions> options,
            ILogger<RabbitMqManager> logger
        )
        {
            _options = options.Value;
            _logger = logger;
        }

        public void SendMessage<T>(T message, MessageType messageType)
        {
            SendMessage(JsonConvert.SerializeObject(message), messageType);
        }

        public void SendMessage(string message, MessageType messageType)
        {
            var routingKey = messageType switch
            {
                MessageType.GivePromoCodeToCustomer => "promocode-factory.give-promocode-to-customer",
                MessageType.NotifyAdminAboutPartnerManagerPromoCode => "promocode-factory.notify-admin-about-partner-manager-promocode",
                _ => null
            };

            if(string.IsNullOrEmpty(routingKey))
                return;

            var factory = new ConnectionFactory()
            {
                HostName = _options.HostName,
                UserName = _options.UserName,
                Password = _options.Password,
                Port = _options.Port,
                VirtualHost = _options.VHost,
            };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();

            channel.ExchangeDeclare(ExchangeName, ExchangeType, true, false, null);

            var body = Encoding.UTF8.GetBytes(message);

            channel.BasicPublish(exchange: ExchangeName, 
                routingKey: routingKey,
                basicProperties: null,
                body: body);
        }
    }
}