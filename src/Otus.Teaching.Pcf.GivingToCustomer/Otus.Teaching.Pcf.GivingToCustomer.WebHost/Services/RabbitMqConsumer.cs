﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services
{
    public class RabbitMqConsumer : BackgroundService
    {
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly ILogger<RabbitMqConsumer> _logger;
        private readonly IServiceProvider _serviceProvider;
        private readonly string _queue;

        public RabbitMqConsumer(
            IOptions<RabbitMqOptions> options,
            ILogger<RabbitMqConsumer> logger,
            IServiceProvider serviceProvider
        )
        {
            var factory = new ConnectionFactory
            {
                HostName = options.Value.HostName,
                UserName = options.Value.UserName,
                Password = options.Value.Password,
                DispatchConsumersAsync = true
            };

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _logger = logger;
            _serviceProvider = serviceProvider;
            _queue = options.Value.Queue;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            if (stoppingToken.IsCancellationRequested)
            {
                _channel.Dispose();
                _connection.Dispose();
                return Task.CompletedTask;
            }

            var consumer = new AsyncEventingBasicConsumer(_channel);

            consumer.Received += ConsumerReceived;

            _channel.BasicConsume(queue: _queue, autoAck: false, consumer: consumer);

            return Task.CompletedTask;
        }

        private async Task ConsumerReceived(object sender, BasicDeliverEventArgs eventArgs)
        {
            var eventName = eventArgs.RoutingKey;
            var message = Encoding.UTF8.GetString(eventArgs.Body.Span);

            try
            {
                await ProcessEvent(eventName, message);
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, "----- ERROR Processing message \"{Message}\"", message);
                _channel.BasicNack(eventArgs.DeliveryTag, multiple: false, true);
                return;
            }

            _channel.BasicAck(eventArgs.DeliveryTag, false);
        }

        private async Task ProcessEvent(string eventName, string message)
        {
            _logger.LogInformation("Processing RabbitMQ event: {EventName}", eventName);

            if (eventName == "promocode-factory.give-promocode-to-customer")
            {
                using var scope = _serviceProvider.CreateScope();
                var promocodesService = scope.ServiceProvider.GetRequiredService<PromocodesService>();
                var mes = JsonConvert.DeserializeObject<GivePromoCodeRequest>(message);
                await promocodesService.GivePromoCodesToCustomersWithPreferenceAsync(mes);
            }
        }
    }
}