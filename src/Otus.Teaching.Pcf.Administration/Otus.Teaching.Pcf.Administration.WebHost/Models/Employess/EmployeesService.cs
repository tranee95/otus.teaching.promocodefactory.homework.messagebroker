﻿using System;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.WebHost.Models.Employess
{
    public class EmployeesService
    {
        private readonly IRepository<Employee> _repository;

        public EmployeesService(IRepository<Employee> repository)
        {
            _repository = repository;
        }

        public async Task<bool> UpdateAppliedPromocodesAsync(Guid id)
        {
            var employee = await _repository.GetByIdAsync(id);

            if (employee == null)
                return false;

            employee.AppliedPromocodesCount++;

            await _repository.UpdateAsync(employee);

            return true;

        }
    }
}